﻿import { Routes, RouterModule } from "@angular/router";
import { PublicLayoutComponent } from "./_layout/public-layout/public-layout.component";
import { PrivateLayoutComponent } from "./_layout/private-layout/private-layout.component";
import { AuthGuard } from "./_guards/auth.guard";
import { RegisterComponent } from "./register";
import { LoginComponent } from "./login";
import { FullLayoutComponent } from "./_layout/full-layout/full-layout.component";
import { PrivacyPolicyComponent } from "./login/privacy-policy/privacy-policy.component";
import { TermsConditionComponent } from "./login/terms-condition/terms-condition.component";
import { ForgotPasswordComponent } from "./forgot-password/forgot-password.component";
import { ResetPasswordComponent } from "./reset-password/reset-password.component";
const appRoutes: Routes = [
  { path: "privacypolicy", component: PrivacyPolicyComponent },
  { path: "termCondition", component: TermsConditionComponent },
  // Public layout
  {
    path: "",
    component: PublicLayoutComponent,
    data: { title: "TOC - Log in or Registration" },
    children: [
      { path: "register", component: RegisterComponent },
      {
        path: "login",
        component: LoginComponent,
        data: { title: "TOC - Log in or Registration" },
      },
      { path: "forgot-password", component: ForgotPasswordComponent },
      { path: "reset-password", component: ResetPasswordComponent },
      { path: "", component: LoginComponent },
    ],
  },
  {
    path: "",
    component: FullLayoutComponent,
    children: [],
  },
  // Private layout
  {
    path: "",
    component: PrivateLayoutComponent,
    children: [
      { path: "logout", component: LoginComponent, canActivate: [AuthGuard] },
      {
        path: "dashboard",
        loadChildren: () =>
          import("../app/content/dashboard/dashboard.module").then(
            (m) => m.DashboardModule
          ),
        data: { title: "TOC - Log in or Registration" },
      },
      {
        path: "depth",
        loadChildren: () =>
          import("../app/depth/depth.module").then((m) => m.DepthModule),
      },
      {
        path: "width",
        loadChildren: () =>
          import("./width/width.module").then((m) => m.WidthModule),
      },
      {
        path: "ibtt",
        loadChildren: () =>
          import("../app/ibtt/ibtt.module").then((m) => m.IbttModule),
      },
      {
        path: "report",
        loadChildren: () =>
          import("../app/content/report/report.module").then(
            (m) => m.ReportModule
          ),
      },
      {
        path: "lifecycle",
        loadChildren: () =>
          import("../app/content/lifecycle/lifecycle.module").then(
            (m) => m.LifecycleModule
          ),
      },
      {
        path: "profile",
        loadChildren: () =>
          import("../app/content/profile/profile.module").then(
            (m) => m.ProfileModule
          ),
      },
      {
        path: "final-list",
        loadChildren: () =>
          import("../app/content/final-list/final-list.module").then(
            (m) => m.FinalListModule
          ),
      },
      {
        path: "seasonality",
        loadChildren: () =>
          import("../app/content/seasonality/seasonality.module").then(
            (m) => m.SeasonalityModule
          ),
      },
      {
        path: "edit-seasonality",
        loadChildren: () =>
          import(
            "../app/content/edit-seasonality/edit-seasonality.module"
          ).then((m) => m.EditSeasonalityModule),
      },
    ],
  },
  // otherwise redirect to home
  { path: "**", redirectTo: "changelog" },
];

export const routing = RouterModule.forRoot(appRoutes, {
  scrollOffset: [0, 0],
  scrollPositionRestoration: "top",
});
