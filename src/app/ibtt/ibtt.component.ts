import { Component, OnInit, ViewChild } from "@angular/core";
import { Title } from "@angular/platform-browser";
import { DataTableDirective } from "angular-datatables";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ToastrService } from "ngx-toastr";

//services
import { HeaderfiltersService } from "src/app/_services/headerfilters.service";
import { IBTTService } from "src/app/_services/ibtt.service";
// const selectData = require("../../../assets//data/forms/form-elements/select.json");
@Component({
  selector: "app-ibtt",
  templateUrl: "./ibtt.component.html",
  styleUrls: ["./ibtt.component.css"],
})
export class IbttComponent implements OnInit {
  @ViewChild(DataTableDirective, { static: false })
  datatableElement: DataTableDirective;
  tableColumns: Array<any> = [];
  tableRows: Array<any> = [];
  dtOptions: Object = {};
  clusterDonerColumns: Array<any> = [];
  clusterDonerData: Array<any> = [];
  replenishNeeded: Object = {};
  donerRowData: Object = {};
  filterJson: Object = {};
  Object: any = Object;
  pendingibtt: any = "";
  lockedWindow: any = "";

  // old
  public breadcrumb: any;

  finalRecommend: any = [];

  constructor(
    private modalService: NgbModal,
    private ibttServices: IBTTService,
    private headerService: HeaderfiltersService,
    private titleService: Title,
    private toastService: ToastrService
  ) {}

  ngOnInit(): void {
    // set Page Title
    this.titleService.setTitle("IBTT | TOC-Inventory ");

    // set Breadcrumb
    this.breadcrumb = {
      links: [
        {
          name: "Home",
          isLink: true,
          link: "/dashboard",
        },
        {
          name: "IBTT",
          isLink: false,
          link: "/ibtt",
        },
      ],
    };

    // data table options
    this.dtOptions = {
      // columns: this.dataColumns,buttons: ["colvis"],
      dom: "lBfrtip",
      buttons: [
        {
          extend: "colvis",
          columns: ":gt(1)",
        },
      ],
      columnDefs: [
        {
          targets: [0, 1], // column index (start from 0)
          orderable: false, // set orderable false for selected columns
        },
      ],
    };

    // page init funtions
    this.getIbttdata();
  }

  ColumnToggle() {}

  chekData(row, i) {
    alert();
    row.qty = i;
  }

  genArray(num) {
    return Array(parseInt(num) + 1);
  }

  // get Ibtt table data
  getIbttdata() {
    var params = {};
    this.ibttServices.getIbttdata(params).subscribe((result: any) => {
      var data = result.data;
      this.pendingibtt = data.pendingCount.Pendingcount;
      this.tableColumns = data.labels;
      this.tableColumns.sort((a, b) => {
        return a.ColumnOrder - b.ColumnOrder;
      });
      this.tableRows = data.lstData;
    });
  }

  // open modal data on + icon
  openModal(content, rowData) {
    const clusterName = rowData.Cluster_Name;
    const apiParam = {
      Cluster_Name: clusterName,
      EAN: rowData.Ean,
    };
    this.donerRowData = rowData;
    this.ibttServices.getIbttDonordata(apiParam).subscribe((result: any) => {
      var data = result.data;
      this.clusterDonerColumns = data["labels"];
      this.clusterDonerData = data["lstData"];
      this.lockedWindow = this.modalService.open(content, {
        windowClass: "animated fadeInDown",
        size: "lg",
      });
    });
  }

  getApproveRejectIbtt(status) {
    const apiParam = {
      Is_Approve: status,
      Ids: String(this.donerRowData["Id"]),
      FinalRecommend: this.finalRecommend,
    };
    this.ibttServices
      .getApproveRejectIbtt(apiParam)
      .subscribe((result: any) => {
        // this.getIbttdata();
        this.lockedWindow.close();
        if (result.success == 1) {
          if (status == 1) {
            this.toastService.success(
              "Your request submitted successfully.",
              "Success!"
            );
          }
        } else {
          this.toastService.error("Something went wrong.", "Error!");
        }
      });
  }

  onCheckboxChangeFn(data, event) {
    if (event.target.checked) {
      this.replenishNeeded[data.Id] = 0;
      var json = {
        donnerId: data.Id,
        From_Location: data.STORE_NAME,
        To_Location: this.donerRowData["Store_code"],
        item: data.Ean,
        Qty: this.replenishNeeded[data.Id],
        Priority: data.Priority,
        Status: data.Status,
      };
      this.finalRecommend.push(json);
    } else {
      delete this.replenishNeeded[data.Id];
      this.finalRecommend.map((item, key) => {
        if (item.donnerId == data.id) {
          this.finalRecommend.splice(key, 1);
        }
      });
    }
  }

  changeReplenishNeeded(donnerId) {
    this.finalRecommend.map((item) => {
      if (item.donnerId == donnerId) {
        item.Qty = parseInt(this.replenishNeeded[donnerId]);
      }
    });
  }

  clearAllFilter() {
    localStorage.removeItem("filterJson");
    localStorage.removeItem("filterId");
    this.getIbttdata();
    this.filterJson = "";
    this.headerService.callFilterData.next(true);
  }
}
