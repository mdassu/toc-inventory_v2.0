import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IbttComponent } from "./ibtt.component";
import { ChartistModule } from "ng-chartist";
import { FormsModule } from "@angular/forms";
import { BreadcrumbModule } from "src/app/_layout/breadcrumb/breadcrumb.module";
import { UiSwitchModule } from "ngx-ui-switch";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { NgSelectModule } from "@ng-select/ng-select";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { RouterModule } from "@angular/router";
import { RecordsTableModule } from "src/app/_layout/records-table/records-table.module";
import { DataTablesModule } from "angular-datatables";

@NgModule({
  declarations: [IbttComponent],
  imports: [
    CommonModule,
    ChartistModule,
    FormsModule,
    BreadcrumbModule,
    UiSwitchModule,
    NgxDatatableModule,
    PerfectScrollbarModule,
    NgSelectModule,
    NgbModule,
    RecordsTableModule,
    DataTablesModule,
    RouterModule.forChild([
      {
        path: "",
        component: IbttComponent,
      },
    ]),
  ],
})
export class IbttModule {}
