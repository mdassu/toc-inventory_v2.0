import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "./../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class GlobalFilterService {
  constructor(private http: HttpClient) {}

  _baseUrl = environment.apiUrl;

  // get filter config api
  getFilterInitialConfig(apiParams?: any) {
    return this.http.post(this._baseUrl + "/TOCPost/Getfilterdata", apiParams);
  }

  // get filter config api
  getInititalFilterData(apiParams?: any) {
    return this.http.post(
      this._baseUrl + "/TOCPost/GetfilterDropdowns",
      apiParams
    );
  }
}
