import { HttpClient } from "@angular/common/http";
import { environment } from "./../../environments/environment";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class WidthService {
  constructor(private http: HttpClient) {}

  _baseUrl = environment.apiUrl;

  // get width data api
  getWidthdata(apiParams?: any) {
    return this.http.post(this._baseUrl + "/TOCPost/GetWidthdata", apiParams);
  }
}
