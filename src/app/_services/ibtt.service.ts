import { environment } from "./../../environments/environment";
import { Injectable } from "@angular/core";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class IBTTService {
  GetClusterUrl = environment.apiUrl + "/TOCPost/GetClusterName"; // (Post Api) All GetClusterName list in Array
  GetPriorityUrl = environment.apiUrl + "/TOCPost/GetPriority"; // (Post Api) All GetPriority list in Array
  GetIbttdataUrl = environment.apiUrl + "/TOCPost/GetIbttdata"; // (Post Api) All GetIbttdata list in Array
  getIbttDonordataUrl = environment.apiUrl + "/TOCPost/GetIbttDonordata";
  getApproveRejectIbtturl = environment.apiUrl + "/TOCPost/ApproveReject";
  constructor(private http: HttpClient) {}
  getPriorityData(apiParams?: any) {
    return this.http.post(this.GetPriorityUrl, apiParams);
  }

  getClusterData(apiParams?: any) {
    return this.http.post(this.GetClusterUrl, apiParams);
  }

  getIbttdata(apiParams?: any) {
    return this.http.post(this.GetIbttdataUrl, apiParams);
  }
  getIbttDonordata(apiParams?: any) {
    return this.http.post(this.getIbttDonordataUrl, apiParams);
  }

  getApproveRejectIbtt(apiParams?: any) {
    return this.http.post(this.getApproveRejectIbtturl, apiParams);
  }

  getFinalListData(apiParams?: any) {
    return this.http.post(this.getApproveRejectIbtturl, apiParams);
  }
}
