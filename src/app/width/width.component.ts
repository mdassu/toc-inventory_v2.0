import { Component, OnInit, ViewChild } from "@angular/core";
import { Title } from "@angular/platform-browser";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { DataTableDirective } from "angular-datatables";

//services
import { WidthService } from "src/app/_services/width.service";
import { HeaderfiltersService } from "src/app/_services/headerfilters.service";

@Component({
  selector: "app-width",
  templateUrl: "./width.component.html",
  styleUrls: ["./width.component.css"],
})
export class WidthComponent implements OnInit {
  @ViewChild(DataTableDirective, { static: false })
  datatableElement: DataTableDirective;
  tableColumns: Array<any> = [];
  tableRows: Array<any> = [];
  dtOptions: any = {};
  rowWidth: Array<any> = [
    { modalStoneName: "110321356", saleRanking: "1", originStock: "5" },
    { modalStoneName: "110321357", saleRanking: "2", originStock: "3" },
    { modalStoneName: "110321358", saleRanking: "3", originStock: "4" },
    { modalStoneName: "110321359", saleRanking: "4", originStock: "9" },
    { modalStoneName: "110321360", saleRanking: "5", originStock: "6" },
  ];
  filterJson: Object = {};
  Object: any = Object;
  pendingwidthCount: any;
  breadcrumb: Object;

  constructor(
    private modalService: NgbModal,
    private headerService: HeaderfiltersService,
    private widthService: WidthService,
    private titleService: Title
  ) {}

  ngOnInit(): void {
    // set Page Title
    this.titleService.setTitle("Width | TOC-Inventory ");

    // set Breadcrumb
    this.breadcrumb = {
      links: [
        {
          name: "Home",
          isLink: true,
          link: "/dashboard",
        },
        {
          name: "Width",
          isLink: false,
          link: "/width",
        },
      ],
    };

    // data table options
    this.dtOptions = {
      dom: "lBfrtip",
      buttons: [
        {
          extend: "colvis",
          columns: ":gt(1)",
        },
      ],
      columnDefs: [
        {
          targets: [0, 1], // column index (start from 0)
          orderable: false, // set orderable false for selected columns
        },
      ],
    };

    // page init funtions
    this.getWidthdata();
  }

  // get width table data
  getWidthdata() {
    var params = {};
    this.widthService.getWidthdata(params).subscribe((result: any) => {
      var data = result.data;
      this.pendingwidthCount = data.pendingCount.Pendingcount;
      this.tableColumns = data.labels;
      this.tableColumns.sort((a, b) => {
        return a.ColumnOrder - b.ColumnOrder;
      });
      this.tableRows = data.lstData;
    });
  }

  // open width record modal
  openModal(content, rowData) {
    this.modalService.open(content, {
      windowClass: "animated fadeInDown",
      size: "lg",
    });
  }

  // change checkbox selection in modal
  onCheckboxChangeFn(data, event) {}

  // clear all filter
  clearAllFilter() {
    this.getWidthdata();
    this.headerService.callFilterData.next(true);
  }
}
