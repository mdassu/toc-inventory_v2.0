import { Title } from "@angular/platform-browser";
import { HeaderfiltersService } from "src/app/_services/headerfilters.service";
import { WidthService } from "./../../_services/width.service";
import { Component, OnInit, ViewChild } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import {
  PerfectScrollbarComponent,
  PerfectScrollbarConfigInterface,
  PerfectScrollbarDirective,
} from "ngx-perfect-scrollbar";
import { TableApiService } from "src/app/_services/table-api.service";
const selectData = require("../../../assets//data/forms/form-elements/select.json");
@Component({
  selector: "app-width",
  templateUrl: "./width.component.html",
  styleUrls: ["./width.component.css"],
})
export class WidthComponent implements OnInit {
  public breadcrumb: any;
  public config: PerfectScrollbarConfigInterface = { wheelPropagation: true };
  data: any;
  options = {
    close: true,
    expand: true,
    minimize: true,
    reload: true,
  };
  temp = [];
  selected = [];
  id: number;
  loadingIndicator: true;
  rows: any;
  editing = {};
  row: any;
  pendingwidthCount: any;
  filterJson: any = "";
  public columns = [
    // { name: "Status", status: true },
    { name: "Stock Location", status: true, element: "stockLocation" },
    { name: "Assortment Group", status: true, element: "assortmentGroup" },
    { name: "Variety Target", status: true, element: "varietyTarget" },
    { name: "Total Options", status: true, element: "totalOptions" },
    { name: "Total Gap", status: true, element: "totalGap" },
    { name: "Variety Color", status: true, element: "" },

    // { name: "Total Inventory", status: true, element: "totalInventory" },
    // { name: "Valid Options", status: true, element: "validOptions" },
    // {
    //   name: "Variety Penetration",
    //   status: true,
    //   element: "varietyPenetration",
    // },
  ];

  //Line Stacked Area Chart
  selectedChannelDatalist = [];
  selectedpartnerDatalist = [];
  selectedregionDatalist = [];
  selectedStateDatalist = [];
  selectedLocationDatalist = [];
  selectedStoreCodeDatalist = [];
  selectedBrandsDatalist = [];

  productBrandDataDatalist = [];
  genderDatalist = [];
  categoryDataList = [];
  subCategoryDatalist = [];
  typeDatalist = [];
  lastNameDatalist = [];
  seasonDatalist = [];
  mrpDatalist = [];
  articalNumberDatalist = [];
  rowData: any;
  rowWidth: any;
  widthData: any;
  //Line Stacked Area Chart
  public lineChartData = lineChartData;
  public lineChartLabels = lineChartLabels;
  public lineChartOptions = lineChartOptions;
  public lineChartColors = lineChartColors;
  public lineChartLegend = lineChartLegend;
  public lineChartType = lineChartType;

  public singleSelectArray = selectData.singleSelectArray;
  @ViewChild(PerfectScrollbarComponent)
  componentRef?: PerfectScrollbarComponent;
  @ViewChild(PerfectScrollbarDirective, { static: true })
  directiveRef?: PerfectScrollbarDirective;

  @BlockUI("addRows") blockUIAddRows: NgBlockUI;
  @BlockUI("rowSelection") blockUIRowSelection: NgBlockUI;
  @BlockUI("basicProgress") blockUIBasicProgress: NgBlockUI;
  @BlockUI("coloredProgress") blockUIColoredProgress: NgBlockUI;
  @BlockUI("basicModals") blockUIBasicModals: NgBlockUI;
  @BlockUI("modalThemes") blockUIModalThemes: NgBlockUI;
  constructor(
    private tableApiservice: TableApiService,
    private modalService: NgbModal,
    private headerService: HeaderfiltersService,
    private widthService: WidthService,
    private titleService: Title
  ) {}
  ngOnInit(): void {
    this.titleService.setTitle("Width | TOC-Inventory ");
    this.breadcrumb = {
      links: [
        {
          name: "Home",
          isLink: true,
          link: "/dashboard",
        },
        {
          name: "Width",
          isLink: false,
          link: "/width",
        },
      ],
    };
    this.tableApiservice.getTableApiData().subscribe((Response) => {
      this.data = Response;
      this.getTabledata();
    });

    this.getWidthdata();

    this.headerService.callFilterDataRef.subscribe((res) => {
      if (res) {
        this.getWidthdata();
        this.headerService.callFilterData.next(false);
      }
    });
  }
  getTabledata() {
    this.rows = this.data.rows;
    this.row = this.data.row;
    this.rowWidth = this.data.widthData;
  }

  addFieldValue() {
    this.rows.push(this.newAttribute);
    this.rows = [...this.rows];
  }

  private newAttribute = {
    status: "",
    newBufferSize: "",
    bufferSize: "",
    sku: "",
    dbmPolicy: "",
    bp: "",
    skuDescription: "",
  };
  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }
  SmallModel(SmallModelContent) {
    this.modalService.open(SmallModelContent, {
      windowClass: "animated fadeInDown",
      size: "sm",
    });
  }
  LargeModel(LargeModelContent, data) {
    this.modalService.open(LargeModelContent, {
      windowClass: "animated fadeInDown",
      size: "lg",
    });
    this.rowData = data;
  }
  getWidthdata() {
    // this.selectedChannelDatalist = this.headerService.selectedChannelDatalist;
    // this.selectedpartnerDatalist = this.headerService.selectedpartnerDatalist;
    // this.selectedregionDatalist = this.headerService.selectedregionDatalist;
    // this.selectedStateDatalist = this.headerService.selectedStateDatalist;
    // this.selectedLocationDatalist = this.headerService.selectedLocationDatalist;
    // this.selectedStoreCodeDatalist = this.headerService.selectedStoreCodeDatalist;
    // this.selectedBrandsDatalist = this.headerService.selectedBrandsDatalist;

    // this.productBrandDataDatalist = this.headerService.productBrandDataDatalist;
    // this.genderDatalist = this.headerService.genderDatalist;
    // this.categoryDataList = this.headerService.categoryDataList;
    // this.subCategoryDatalist = this.headerService.subCategoryDatalist;
    // this.typeDatalist = this.headerService.typeDatalist;
    // this.lastNameDatalist = this.headerService.lastNameDatalist;
    // this.seasonDatalist = this.headerService.seasonDatalist;
    // this.mrpDatalist = this.headerService.mrpDatalist;
    // this.articalNumberDatalist = this.headerService.articalNumberDatalist;

    // const brandsCode = [];

    // this.headerService.selectedStoreCodeDatalist.forEach(function (task) {
    //   let n = task.substring(task.lastIndexOf("(") + 1, task.lastIndexOf(")"));
    //   brandsCode.push(n);
    // });
    // const apiParam = {
    //   channel: this.headerService.selectedChannelDatalist.join(","),
    //   partners: this.headerService.selectedpartnerDatalist.join(","),
    //   region: this.headerService.selectedregionDatalist.join(","),
    //   state: this.headerService.selectedStateDatalist.join(","),
    //   city: this.headerService.selectedLocationDatalist.join(","),
    //   storecode: brandsCode.join(","),
    //   locationbrand: this.headerService.selectedBrandsDatalist.join(","),
    //   productbrand: this.headerService.productBrandDataDatalist.join(","),
    //   gender: this.headerService.genderDatalist.join(","),
    //   category: this.headerService.categoryDataList.join(","),
    //   subcategory: this.headerService.subCategoryDatalist.join(","),
    //   type: this.headerService.typeDatalist.join(","),
    //   lastname: this.headerService.lastNameDatalist.join(","),
    //   season: this.headerService.seasonDatalist.join(","),
    //   mrp: this.headerService.mrpDatalist.join(","),
    //   articalno: this.headerService.articalNumberDatalist.join(","),
    // };

    if (localStorage.getItem("filterJson")) {
      this.filterJson = localStorage.getItem("filterJson");
      var filterJson = JSON.parse(this.filterJson);
      //Product
      // if (filterJson["productbrand"] != "") {
      //   this.productBrandDataDatalist = filterJson["productbrand"].split(",");
      // }
      // if (filterJson["gender"] != "") {
      //   this.genderDatalist = filterJson["gender"].split(",");
      // }
      // if (filterJson["category"] != "") {
      //   this.categoryDataList = filterJson["category"].split(",");
      // }
      // if (filterJson["subcategory"] != "") {
      //   this.subCategoryDatalist = filterJson["subcategory"].split(",");
      // }
      // if (filterJson["type"] != "") {
      //   this.typeDatalist = filterJson["type"].split(",");
      // }
      // if (filterJson["lastname"] != "") {
      //   this.lastNameDatalist = filterJson["lastname"].split(",");
      // }
      // if (filterJson["season"] != "") {
      //   this.seasonDatalist = filterJson["season"].split(",");
      // }
      // if (filterJson["mrp"] != "") {
      //   this.mrpDatalist = filterJson["mrp"].split(",");
      // }
      // if (filterJson["articalno"] != "") {
      //   this.articalNumberDatalist = filterJson["articalno"].split(",");
      // }

      // //Location
      // if (filterJson["channel"] != "") {
      //   this.selectedChannelDatalist = filterJson["channel"].split(",");
      // }
      // if (filterJson["partners"] != "") {
      //   this.selectedpartnerDatalist = filterJson["partners"].split(",");
      // }
      // if (filterJson["region"] != "") {
      //   this.selectedregionDatalist = filterJson["region"].split(",");
      // }
      // if (filterJson["state"] != "") {
      //   this.selectedStateDatalist = filterJson["state"].split(",");
      // }
      // if (filterJson["city"] != "") {
      //   this.selectedLocationDatalist = filterJson["city"].split(",");
      // }
      // if (filterJson["locationbrand"] != "") {
      //   this.selectedBrandsDatalist = filterJson["locationbrand"].split(",");
      // }

      if (filterJson["category"] != "") {
        this.productBrandDataDatalist = filterJson["category"].split(",");
      }
      if (filterJson["agCode"] != "") {
        this.genderDatalist = filterJson["agCode"].split(",");
      }
      if (filterJson["subcategory"] != "") {
        this.categoryDataList = filterJson["subcategory"].split(",");
      }
      if (filterJson["familyName"] != "") {
        this.subCategoryDatalist = filterJson["familyName"].split(",");
      }
      if (filterJson["setting"] != "") {
        this.typeDatalist = filterJson["setting"].split(",");
      }
      if (filterJson["designNo"] != "") {
        this.lastNameDatalist = filterJson["designNo"].split(",");
      }
      if (filterJson["carats"] != "") {
        this.seasonDatalist = filterJson["carats"].split(",");
      }
      if (filterJson["salePrice"] != "") {
        this.mrpDatalist = filterJson["salePrice"].split(",");
      }
      if (filterJson["modelId"] != "") {
        this.articalNumberDatalist = filterJson["modelId"].split(",");
      }

      if (filterJson["quality"] != "") {
        this.articalNumberDatalist = filterJson["quality"].split(",");
      }

      //Location
      if (filterJson["region"] != "") {
        this.selectedregionDatalist = filterJson["region"].split(",");
      }
      if (filterJson["state"] != "") {
        this.selectedStateDatalist = filterJson["state"].split(",");
      }
      if (filterJson["city"] != "") {
        this.selectedLocationDatalist = filterJson["city"].split(",");
      }
      if (filterJson["storeCode"] != "") {
        this.selectedStoreCodeDatalist = filterJson["storeCode"].split(",");
      }
    }
    var params = {};
    if (localStorage.getItem("filterJson")) {
      params = JSON.parse(localStorage.getItem("filterJson"));
    }
    this.widthService.getWidthdata(params).subscribe((result: any) => {
      this.pendingwidthCount = result.data.widthPendingDecision.pendingcount;
      this.rows = result.data.widthList;
    });
  }

  clearAllFilter() {
    localStorage.removeItem("filterJson");
    localStorage.removeItem("filterId");
    this.getWidthdata();
    this.filterJson = "";
    this.headerService.callFilterData.next(true);
  }
}

export const lineChartData: Array<any> = [
  { data: [0, 20, 11, 19, 10, 22, 9], label: "Series A" },
  { data: [28, 48, 35, 29, 46, 27, 60], label: "Series B" },
  { data: [56, 70, 55, 46, 67, 52, 70], label: "Series C" },
  { data: [60, 75, 65, 56, 77, 62, 80], label: "Series D" },
  { data: [45, 60, 10, 25, 90, 45, 60], label: "Inventory in Store" },
  { data: [35, 70, 40, 75, 80, 55, 66], label: "Inventory in Transit" },
];
export const lineChartLabels: Array<any> = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
];
export const lineChartOptions: any = {
  animation: {
    duration: 1000, // general animation timebar
    easing: "easeOutBack",
  },
  hover: {
    animationDuration: 1000, // duration of animations when hovering an item
  },
  responsiveAnimationDuration: 1000, // animation duration after a resize
  responsive: true,
  maintainAspectRatio: false,
  scales: {
    xAxes: [
      {
        display: true,
        ticks: {
          padding: 4,
        },
        gridLines: {
          color: "#f3f3f3",
          drawTicks: false,
        },
        scaleLabel: {
          display: true,
          labelString: "Month",
        },
      },
    ],
    yAxes: [
      {
        display: true,
        gridLines: {
          color: "#f3f3f3",
          drawTicks: false,
        },
        ticks: {
          padding: 4,
        },
        scaleLabel: {
          display: true,
          labelString: "Value",
        },
      },
    ],
  },
};
export const lineChartColors: Array<any> = [
  {
    backgroundColor: "rgb(138,233,204,0.5)",
    borderColor: "rgb(138,233,204,1)",
    pointBackgroundColor: "rgb(138,233,204,1)",
    pointBorderColor: "rgb(138,233,204,1)",
  },
  {
    backgroundColor: "rgb(68,186,239,0.8)",
    borderColor: "rgb(168,186,239,1)",
    pointBackgroundColor: "rgb(168,186,239,1)",
    pointBorderColor: "rgb(168,186,239,1)",
  },
  {
    backgroundColor: "#ffffe0",
    borderColor: "#ffd772",
    pointBackgroundColor: "#ffd772",
    pointBorderColor: "#ffd772",
  },

  {
    backgroundColor: "#ffe8eb",
    borderColor: "#ff7e90",
    pointBackgroundColor: "#ff7e90",
    pointBorderColor: "#ff7e90",
  },
  {
    backgroundColor: "transparent",
    borderColor: "#b3b3b3",
    pointBackgroundColor: "#b3b3b3",
    pointBorderColor: "#b3b3b3",
  },
  {
    backgroundColor: "transparent",
    borderColor: "#b3b3b3",
    pointBackgroundColor: "#b3b3b3",
    pointBorderColor: "#b3b3b3",
  },
];
export const lineChartLegend = true;
export const lineChartType = "line";
