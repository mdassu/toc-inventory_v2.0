import { NgModule } from "@angular/core";
import { CommonModule, DatePipe } from "@angular/common";
import { RouterModule } from "@angular/router";
import { EcommerceComponent } from "./ecommerce/ecommerce.component";
import { ChartistModule } from "ng-chartist";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { FormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ChartsModule } from "ng2-charts";
import { SalesComponent } from "./sales/sales.component";
import { BlockUIModule } from "ng-block-ui";
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { NgSelectModule } from "@ng-select/ng-select";
import { BreadcrumbModule } from "src/app/_layout/breadcrumb/breadcrumb.module";

@NgModule({
  imports: [
    CommonModule,
    ChartistModule,
    FormsModule,
    ChartsModule,
    BreadcrumbModule,
    NgxDatatableModule,
    PerfectScrollbarModule,
    NgSelectModule,
    NgbModule,

    RouterModule.forChild([
      {
        path: "ecommerce",
        component: EcommerceComponent,
      },
      {
        path: "",
        component: SalesComponent,
      },
    ]),
  ],
  declarations: [EcommerceComponent, SalesComponent],

  exports: [RouterModule],
  providers: [DatePipe]
})
export class DashboardModule {}
