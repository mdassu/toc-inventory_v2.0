import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DepthComponent } from "./depth.component";
import { RouterModule } from "@angular/router";
import { BlockUIModule } from "ng-block-ui";
import { NgSelectModule } from "@ng-select/ng-select";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { BreadcrumbModule } from "src/app/_layout/breadcrumb/breadcrumb.module";
import { FormsModule } from "@angular/forms";
import { ChartsModule } from "ng2-charts";
import { ChartistModule } from "ng-chartist";
import { UiSwitchModule } from "ngx-ui-switch";
import { RecordsTableModule } from "src/app/_layout/records-table/records-table.module";

@NgModule({
  imports: [
    CommonModule,
    ChartistModule,
    FormsModule,
    ChartsModule,
    BreadcrumbModule,
    UiSwitchModule,
    NgxDatatableModule,
    PerfectScrollbarModule,
    NgSelectModule,
    NgbModule,
    RecordsTableModule,
    RouterModule.forChild([
      {
        path: "",
        component: DepthComponent,
      },
    ]),
  ],
  declarations: [DepthComponent],
})
export class DepthModule {}
