import { Component, OnInit, ViewChild } from "@angular/core";
import { Title } from "@angular/platform-browser";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { DataTableDirective } from "angular-datatables";

// services
import { DepthService } from "./../_services/depth.service";
import { HeaderfiltersService } from "src/app/_services/headerfilters.service";

@Component({
  selector: "app-depth",
  templateUrl: "./depth.component.html",
  styleUrls: ["./depth.component.css"],
})
export class DepthComponent implements OnInit {
  @ViewChild(DataTableDirective, { static: false })
  datatableElement: DataTableDirective;
  tableColumns: Array<any> = [];
  tableRows: Array<any> = [];
  dtOptions: Object = {};
  filterJson: Object = {};
  Object: any = Object;
  pendingdepthCount: any;
  breadcrumb: Object;

  public lineChartData = lineChartData;
  public lineChartLabels = lineChartLabels;
  public lineChartOptions = lineChartOptions;
  public lineChartColors = lineChartColors;
  public lineChartLegend = lineChartLegend;
  public lineChartType = lineChartType;

  constructor(
    private modalService: NgbModal,
    private headerService: HeaderfiltersService,
    private depthService: DepthService,
    private titleService: Title
  ) {}
  ngOnInit(): void {
    // set Page Title
    this.titleService.setTitle("Depth | TOC-Inventory ");

    // set Breadcrumb
    this.breadcrumb = {
      links: [
        {
          name: "Home",
          isLink: true,
          link: "/dashboard",
        },
        {
          name: "Depth",
          isLink: false,
          link: "/depth",
        },
      ],
    };

    // data table options
    this.dtOptions = {
      // columns: this.dataColumns,buttons: ["colvis"],
      dom: "lBfrtip",
      buttons: [
        {
          extend: "colvis",
          columns: ":gt(1)",
        },
      ],
      columnDefs: [
        {
          targets: [0, 1], // column index (start from 0)
          orderable: false, // set orderable false for selected columns
        },
      ],
    };

    // page init funtions
    this.getDepthdata();
  }

  // get depth table data
  getDepthdata() {
    var params = {};
    this.depthService.getDepthdata(params).subscribe((result: any) => {
      var data = result.data;
      this.pendingdepthCount = data.pendingCount.Pendingcount;
      this.tableColumns = data.labels;
      this.tableColumns.sort((a, b) => {
        return a.ColumnOrder - b.ColumnOrder;
      });
      this.tableRows = data.lstData;
    });
  }

  // open Modal for depth record
  openModal(content, rowData) {
    if (rowData % 2 == 0) {
      this.modalService.open(content, {
        windowClass: "animated fadeInDown",
        size: "sm",
      });
    } else {
      this.modalService.open(content, {
        windowClass: "animated fadeInDown",
        size: "sm",
      });
    }
  }

  clearAllFilter() {
    this.getDepthdata();
    this.headerService.callFilterData.next(true);
  }
}

export const lineChartData: Array<any> = [
  { data: [0, 10, 8, 17, 8, 15, 7], label: "Series A" },
  { data: [10, 20, 15, 25, 19, 22, 18], label: "Series B" },
  { data: [28, 48, 35, 29, 46, 27, 60], label: "Series C" },
  { data: [56, 70, 55, 46, 67, 52, 70], label: "Series D" },
  { data: [60, 75, 65, 56, 77, 62, 80], label: "Series E" },
  { data: [45, 60, 10, 25, 90, 45, 60], label: "Inventory in Store" },
];
export const lineChartLabels: Array<any> = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
];
export const lineChartOptions: any = {
  animation: {
    duration: 1000, // general animation timebar
    easing: "easeOutBack",
  },
  hover: {
    animationDuration: 1000, // duration of animations when hovering an item
  },
  responsiveAnimationDuration: 1000, // animation duration after a resize
  responsive: true,
  maintainAspectRatio: false,
  scales: {
    xAxes: [
      {
        display: true,
        ticks: {
          padding: 4,
        },
        gridLines: {
          color: "#f3f3f3",
          drawTicks: false,
        },
        scaleLabel: {
          display: true,
          labelString: "Month",
        },
      },
    ],
    yAxes: [
      {
        display: true,
        gridLines: {
          color: "#f3f3f3",
          drawTicks: false,
        },
        ticks: {
          padding: 4,
        },
        scaleLabel: {
          display: true,
          labelString: "Value",
        },
      },
    ],
  },
};
export const lineChartColors: Array<any> = [
  {
    backgroundColor: "#4D4D4D",
    borderColor: "transparent",
    pointBackgroundColor: "transparent",
    pointBorderColor: "transparent",
  },
  {
    backgroundColor: "#FF0000",
    borderColor: "transparent",
    pointBackgroundColor: "transparent",
    pointBorderColor: "transparent",
  },
  {
    backgroundColor: "#FFFF00",
    borderColor: "transparent",
    pointBackgroundColor: "transparent",
    pointBorderColor: "transparent",
  },
  {
    backgroundColor: "#00FF00",
    borderColor: "transparent",
    pointBackgroundColor: "transparent",
    pointBorderColor: "transparent",
  },

  {
    backgroundColor: "#0000FF",
    borderColor: "transparent",
    pointBackgroundColor: "transparent",
    pointBorderColor: "transparent",
  },
  {
    backgroundColor: "transparent",
    borderColor: "#b3b3b3",
    pointBackgroundColor: "#b3b3b3",
    pointBorderColor: "#b3b3b3",
  },
];
export const lineChartLegend = true;
export const lineChartType = "line";
